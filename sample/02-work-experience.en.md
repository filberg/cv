::: {#job .job .history}
Work Experience
---------------

::: {.job-sub}
### 2199, Present - Programmer

#### Some Lame Company - Big City, ZZ

Since 1999 (or was it closer to 2199?) I\'ve been programming
stuff for this Lame Company. I have a problem with authority.
I believe I'm special, that somehow the rules do not apply to me.
Obvioulsy, they think I am mistaken.

-   ##### This the first heading in a list:

	On the first day of Christmas my true love sent to me: a partridge
	in a pear tree.

-   ##### Another heading:

    On the second day of Christmas my true love sent to me: 2 Turtle
	Doves and a Partridge in a Pear Tree.

-   ##### Yet another heading:

	On the twelfth day of Christmas my true love sent to me:
	12 Drummers Drumming, 11 Pipers Piping, Lords a Leaping, 9 Ladies
	Dancing, 8 Maids a Milking, 7 Swans a Swimming, 6 Geese a Laying,
	5 Golden Rings, 4 Calling Birds, 3 French Hens, 2 Turtle Doves 
	and a Partridge in a Pear Tree.
:::

::: {.job-sub}
### 1985, 1991 - Paperboy

#### A Tari Co. - Friendly Neighborhood, US

I have been delivering newspapers in the whole neighborhood
each day at 6AM for 6 years, until I crashed into a post box.
This job taught me how to avoid obstacles in life.
	
:::

:::
