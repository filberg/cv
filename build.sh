#!/bin/bash

#
#    Copyright (c) 2021 Filberg <https://www.filberg.eu>
#
#    This file is part of "Filberg's CV template".
#
#    "Filberg's CV template" is free software: you can redistribute
#    it and/or modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    "Filberg's CV template" is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with "Filberg's CV template".
#    If not, see <https://www.gnu.org/licenses/>.
#

scriptdir=$(dirname "$0")

pagesize="A4"
margin_t=0
margin_r=0
margin_b=0
margin_l=0
html_resources="$scriptdir/img $scriptdir/js $scriptdir/styles"
pdoc_template="$scriptdir/templates/template.html"
pdoc_page_template="$scriptdir/templates/page.templ.html"
dir_outdir="cv_out"
dir_out_html="$dir_outdir/html"
strings_base="$scriptdir/base/strings.yaml"
strings_override="strings.yaml"
varname_pagenum="vars_pagenumber"
varname_pagecnt="vars_pagecount"
output_filename="cv"

usg="
    USAGE:

       $0 OUTPUT_FORMAT CONTENTS_DIR

     OUTPUT_FORMAT : html | pdf

     CONTENTS_DIR  : a directory containing at least one markdown file
                     and (optionally) a YAML resource file named '$strings_override'

                     Each markdown file found in CONTENTS_DIR is used
                     to produce a single page in the output document.
                     To enure proper page sorting, prepend the page number
                     to each page's file name, e.g.:
                           \"01-first-page.md\"
                           \"02-second-page.md\"
                           \"...\"

                     If CONTENTS_DIR contains a YAML file named '$strings_override',
                     it is used to override template variables definition;
                     otherwise, the default variables are used, as defined in
                     the template file $strings_base.
"

_printerr()
{
    echo "$1" >&2
}

# $1 = filename
_confirm_overwrite()
{
    [ -f "$1" ] &&
	{
	    printf "\nWARNING:\n"
	    printf "The output file \"$1\" already exists and will be overwritten\n"
	    printf "Do you want to continue? [Y/n]"

	    read ans

	    case "$ans" in
		N|n)
		    printf "Aborted.\n"
		    return 9
		    ;;
		*);;
	    esac
	}
    return 0
}

# $1 = input
# $2 = output
# $3 = page number
# $4 = page count
# $5 = yaml resources
_build_html_page()
{
    pandoc \
	--template="$pdoc_page_template" \
	--from=markdown \
	--to=html \
	--strip-comments \
	--variable="$varname_pagenum:$3" \
	--variable="$varname_pagecnt:$4" \
	"$5" \
	"$1" \
	-o "$2"
}

# $1 output dir
_copy_html_resx()
{
    for r in $html_resources
    do
	cp -r "$r" "$1"
    done
}

# $1 = contents dir
_get_html_outname()
{
    srcname=$(basename "$1")
    echo "$dir_out_html/${output_filename}_$srcname.html"
}

# $1 = contents dir
_get_pdf_outname()
{
    srcname=$(basename "$1")
    echo "$dir_outdir/${output_filename}_$srcname.pdf"
}


# $1 = contents dir
_build_html()
{
    [ -d "$1" ] ||
	{
	    _printerr "ERROR: $1 is not a valid directory"
	    return 7
	}

    mkdir -p "$dir_out_html"

    outpath=$(_get_html_outname "$1")

    _confirm_overwrite "$outpath" ||
	return $?

    ls -1 "$1"/*\.md 1>/dev/null 2>&1 ||
	{
	    _printerr "ERROR: \"$1\" does not contain any markdown file"
	    return 64
	}

    pages=("$1"/*\.md)
    
    declare -a html_pages
    declare -a opt_include

    pagecount=${#pages[@]}

    resxfile="$1/$strings_override"
    
    [ -f "$resxfile" ] ||
	{
	    _printerr "WARNING: '$strings_override' file not found. Default template variables will be used"
	    resxfile="$strings_base"
	}

    pagenum=1

    for p in "${pages[@]}"
    do
	pgout="$(mktemp).html"
	html_pages+=("$pgout")
	opt_include+=(--include-after-body "$pgout")

	_build_html_page \
	    "$p" \
	    "$pgout" \
	    "$pagenum" \
	    "$pagecount" \
	    "$resxfile" ||
	    {
		_printerr "FAILED to build html page for $p"
		return 5
	    }

	pagenum=$((pagenum+1))

    done
    
    pandoc \
	--template="$pdoc_template" \
	--from=markdown \
	--to=html \
	--strip-comments \
	"${opt_include[@]}" \
	"$resxfile" \
	-o "$outpath"

    ec=$?

    for tmppag in $html_pages
    do
	rm -f "$tmppag"
    done

    [ ! $ec -eq 0 ] &&
	{
	    _printerr "FAILED to build final html document"
	    _printerr "pandoc exited with code $ec"
	    return  $ec
	}

    _copy_html_resx "$dir_out_html" ||
	{
	    _printerr "FAILED to copy html resources to output directory"
	    return 7
	}

    echo "Output html file created at $outpath"
    return 0
}

# $1 = contents dir
_build_pdf()
{
    _build_html "$1" ||
	return $?

    infile=$(_get_html_outname "$1")
    outpath=$(_get_pdf_outname "$1")

    _confirm_overwrite "$outpath" ||
	return $?

    infile=$(readlink -f "$infile") ||
	{
	    _printerr "FAILED to canonicalize path for $infile"
	    return 17
	}

    set -x
    wkhtmltopdf \
	--enable-local-file-access \
	--page-size "$pagesize" \
	--margin-top $margin_t \
	--margin-right $margin_r \
	--margin-bottom $margin_b \
	--margin-left $margin_l \
	"file://$infile?mode=pdf" \
	"$outpath" ||
	{
	    _printerr "FAILED: wkhtmltopdf failed with error $?"
	    return 32
	}
    set +x

    echo "Final PDF file created at $outpath"
    return 0
}

_clean()
{
    [ -d "$dir_outdir" ] &&
	rm -r "$dir_outdir"
}

[ ! $# -eq 2 ] &&
    {
	_printerr "$usg"
	exit 1
    }

echo "$2"

case "$1" in

    html|HTML)
	_build_html "$2"
	;;

    pdf|PDF)
	_build_pdf "$2"
	;;

    -h|--help)
	_printerr "$usg"
	exit 0
	;;

    *)
	_printerr "Invalid output format: $1"
	_printerr "$usg"
	exit 1
	;;
esac
