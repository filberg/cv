#!/usr/bin/env python3

#
#    Copyright (c) 2021 Filberg <https://www.filberg.eu>
#
#    This file is part of "Filberg's CV template".
#
#    "Filberg's CV template" is free software: you can redistribute
#    it and/or modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    "Filberg's CV template" is distributed in the hope that it will be
#    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with "Filberg's CV template".
#    If not, see <https://www.gnu.org/licenses/>.
#


import sys
import os
import shutil
import subprocess

from pathlib import Path

pandoc_bin = "pandoc"

pagesize = "A4"
margin_t = 0
margin_r = 0
margin_b = 0
margin_l = 0

scriptdir = ""
dir_outdir = "cv_out"

templates_dir = \
    os.path.join(
        scriptdir,
        "templates")

pdoc_template = \
    os.path.join(
        templates_dir,
        "template.html")

doc_page_template = \
    os.path.join(
        templates_dir,
        "page.templ.html")

dir_out_html = \
    os.path.join(
        dir_outdir,
        "html")

dir_base = \
    os.path.join(
        scriptdir,
        "base")

strings_base = \
    os.path.join(
        dir_base,
        "strings.yaml")

html_resources = \
    [
        "img",
        "js",
        "styles"
    ]

strings_override = "strings.yaml"
varname_pagenum = "vars_pagenumber"
varname_pagecnt = "vars_pagecount"
output_filename = "cv"


class Status:

    def __init__(
            self,
            msg,
            code):

        self.code = code
        self.message = msg


def print_err(msg):
    print(msg, file=sys.stderr)


def confirm_overwrite(fname):

    f = Path(fname)

    if not f.is_file():
        return True

    print()
    print("WARNING:")
    print(
        "The output file "
        + fname
        + " already exists and will be overwritten")
    print("Do you want to continue? [Y/n] ")

    ans = input()

    return ans.lower != "n" \
        and ans.lower != "no"


def getopt_variable(key, val):
    return "--variable={k}:{v}".format(key, value)


def getopt_template(t):
    return "--template={}".format(t)


def build_html_page(
        infile,
        outfile,
        pagenum,
        pagecount,
        stringsfile):

    return subprocess.run(
        [
            pandoc_bin,
            getopt_template(doc_page_template),
            "--from=markdown",
            "--to=html",
            "--strip-comments",
            getopt_variable(varname_pagenum, pagenum),
            getopt_variable(varname_pagecnt, pagecount),
            stringsfile,
            infile,
            "-o", outfile
        ]
    ).returncode


def copy_html_resx(outdir):

    try:

        for r in html_resources:
            dst = os.path.join(outdir, r)

            if os.path.isdir(dst):
                os.rmdir(dst)

            shutil.copytree(r, dst)

        return True

    except OSError:
        return False


def get_outname(
        contents_dir,
        output_dir,
        extension):

    srcname = \
        os.path.basename(contents_dir)

    fname = \
        "{base}_{name}.{ext}".format(
            base=output_filename,
            name=srcname,
            ext=extension)

    return os.path.join(
        output_dir,
        fname)


def get_html_outname(contents_dir):
    return get_outname(
        contents_dir,
        dir_out_html,
        "html")


def get_pdf_outname(contents_dir):
    return get_outname(
        contents_dir,
        dir_outdir,
        "pdf")


def build_html(contents_dir):

    if not os.path.isdir(contents_dir):
        return Status(
            f"ERROR: {contents_dir} is not a valid directory",
            7)

    os.makedirs(
        dir_out_html,
        exist_ok=True)

    outpath = get_html_outname(contents_dir)

    if not confirm_overwrite(outpath):
        return Status("Aborted.", 9)
