
### Summary ###

This is an HTML template system to produce a CV/resumé both in PDF and HTML format, starting from markdown files.

The html version produces a static webpage, optimized for both desktop and mobile browsing.

To customize the template with your data, use a custom `strings.yaml` file, following [the base template](base/strings.yaml).

To write your customized CV info, follow [the sample markdown pages](sample/).

Run the build script on a directory containing all your custom markdwon pages and `strings.yaml` file:
```
./build.sh OUTPUT_FORMAT CONTENTS_DIR
```

Output is stored in `cv_out` subdirectory created in the current working directory.

See [the sample webpage](https://filberg.eu/public/cv_sample/cv_sample.html) 
or [the sample pdf](https://filberg.eu/public/cv_sample/cv_sample.pdf) for a rough idea of the final outcome.


### Dependencies ###

[pandoc](https://www.pandoc.org) is required for building both the html and the pdf version.

To install pandoc on Debian (or any derivative):

```
sudo apt install pandoc
```

(optional) [wkhtmltopdf](https://wkhtmltopdf.org/) is only needed to create the pdf version of the CV.

To install wkhtmltopdf on Debian (or any derivative):
```
sudo apt install wkhtmltopdf
```


### Usage ###
```
./build.sh OUTPUT_FORMAT CONTENTS_DIR

     OUTPUT_FORMAT : html | pdf

     CONTENTS_DIR  : a directory containing at least one markdown file
                     and (optionally) a YAML resource file named 'strings.yaml'

                     Each markdown file found in CONTENTS_DIR is used
                     to produce a single page in the output document.
                     To enure proper page sorting, prepend the page number
                     to each page's file name, e.g.:
                           "01-first-page.md"
                           "02-second-page.md"
                           "..."

                     If CONTENTS_DIR contains a YAML file named 'strings.yaml',
                     it is used to override template variables definition;
                     otherwise, the default variables are used, as defined in
                     the template file ./base/strings.yaml.
```
