/*
    Copyright (c) 2021 Filberg <https://www.filberg.eu>

    This file is part of "Filberg's CV template".

    "Filberg's CV template" is free software: you can redistribute
    it and/or modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    "Filberg's CV template" is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with "Filberg's CV template".
    If not, see <https://www.gnu.org/licenses/>.
*/

var QUERY_KEY_MODE = "mode";
var MODE_NAME_PREVIEW = "preview";
var MODE_NAME_PDF = "pdf";
var MODE_NAME_BASE = "base";

var SUBDIR_STYLES = "styles";

var StyleMode = function(name, styles)
{
    this.Name = name;
    this.Styles = styles;
}

var Styles = [];

Styles[MODE_NAME_BASE] =
    new StyleMode(
	MODE_NAME_BASE,
	["cv.css"]);

Styles[MODE_NAME_PDF] =
    new StyleMode(
	MODE_NAME_PDF,
	["cv.css", "pdf.css"]);

Styles[MODE_NAME_PREVIEW] =
    new StyleMode(
	MODE_NAME_PREVIEW,
	["cv.css", "pdf.css", "preview.css"]);

function GetUrlQueryValue(key)
{
    if(! window.URLSearchParams === undefined)
    {
	var usp = new URLSearchParams(window.location.search);
	if (!usp)
	    return null;
	
	return usp.get(key);
    }

    // if URLSearchParams is not supported:

    console.log(window.location.search);

    if (! window.location.search)
	return null;

    var splt = window.location.search.replace('?', '').split('&');
    
    for(var i=0; i<splt.length; i++)
    {
	var kvp = splt[i].split('=');
	if (kvp.length == 2
	    && kvp[0] == key)
	    return decodeURIComponent(
		kvp[1].replace(/\+/g, ' '));

    }

    return null;
}

function GetCurrentMode()
{
    var name = GetUrlQueryValue(QUERY_KEY_MODE);
    if (name == null)
	return null;

    return Styles[name];
}

function AppendStyle(style)
{
    var l = document.createElement("link");
    l.href = SUBDIR_STYLES + "/" + style;
    l.rel = "stylesheet";
    l.type = "text/css";

    var h = document.getElementsByTagName("head")[0];
    h.appendChild(l);
}

function ExplicitStyle(mode)
{
    for(var i=0; i<mode.Styles.length; i++)
	AppendStyle(
	    mode.Styles[i]);
}

function ApplyStyleMode()
{
    var curMode = GetCurrentMode();

    if (curMode == null)
	curMode = Styles[MODE_NAME_BASE];

    if (document.styleSheets.length < 2
	&& curMode != null
        && curMode.Name != MODE_NAME_BASE)
    {
	// alternate stylesheet were not loaded
	// explicitly set them
	ExplicitStyle(curMode);
	return;
    }

    for (var idx=0; idx < document.styleSheets.length; idx++)
    {
	var s = document.styleSheets[idx];
	s.disabled =
	    s.title != null
	    && s.title != curMode.Name;
    }
}
